export const URL_SECURITY = 'WsSecurity.svc';
export const URL_DINERS = 'WsDiners.svc';
export const URL_DELIVERY = 'WsDelivery.svc';
export const URL_IMAGE_PRODUCT = 'Fotos';
export const URL_IMAGE_RESTAURANTS = 'RestaurantImage';
