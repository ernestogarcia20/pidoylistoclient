/**
 * AUTH
 */

export const AUTHENTICATOR = 'Authenticator';
export const LOGIN = 'Login';
export const SIGN_UP = 'SignUp';
/**
 * DASHBOARD
 */
export const DASHBOARD = 'Dashboard';
export const DRAWER = 'Drawer';
export const COMPANIES_LIST = 'CompaniesList';
export const PRODUCTS = 'Products';
export const PRODUCT_SELECTED  = 'ProductSelect';
