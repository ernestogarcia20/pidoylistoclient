import React from 'react';
import {StyleSheet, Text} from 'react-native';
// import {fontMaker} from './typografy';

const TextComponent = ({children, fontSize, color, weight = 'bold', align = 'center', style}) => {
  const styles = StyleSheet.create({
    text: {
      fontSize,
      color,
      textAlign: align,
      fontWeight: weight,
    },
  });
  return <Text style={[styles.text, {...style}]}>{children}</Text>;
};

export default TextComponent;
