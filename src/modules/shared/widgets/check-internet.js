import React, { Component } from 'react';
import { View } from 'react-native';
import NetInfo, { NetInfoSubscription } from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import { SET_INTERNET } from '../../../store/reducers/check-internet';

class CheckInternet extends Component {
  state = { internet: true };

  subscription: NetInfoSubscription | null = null;

  componentDidMount() {
    const { dispatch } = this.props;
    NetInfo.fetch().then(e => {
      this.setDispatch(dispatch, SET_INTERNET, e.isConnected);
    });
    this.subscription = NetInfo.addEventListener(this.handleConnectionChange);
  }

  componentWillUnmount() {
    this.subscription && this.subscription();
  }

  handleConnectionChange = state => {
    const { dispatch } = this.props;
    this.setDispatch(dispatch, SET_INTERNET, state.isConnected);
  };

  setDispatch = (dispatch, type, data) => {
    dispatch({
      type,
      payload: data,
    });
  };

  render() {
    return <View />;
  }
}
const mapStateToProps = ({ checkInternet: { checkInternet } }) => ({
  checkInternet,
});

export default connect(mapStateToProps)(CheckInternet);
