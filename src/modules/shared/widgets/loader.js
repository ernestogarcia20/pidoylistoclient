import React from 'react';
import {StyleSheet, ActivityIndicator, View, Dimensions} from 'react-native';
import Pallete from '../assets/palette';

const Loader = () => (
  <View style={styles.loader}>
    <ActivityIndicator color={Pallete.primaryColor} size="large" />
  </View>
);

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  loader: {
    height: height * 0.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Loader;
