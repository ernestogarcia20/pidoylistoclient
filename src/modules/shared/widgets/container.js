import React from 'react';
import {SafeAreaView, StyleSheet, Keyboard, StatusBar, ActivityIndicator, View} from 'react-native';
import Pallete from '../assets/palette';

const Container = ({children, style, isLoading}) => (
  <SafeAreaView onStartShouldSetResponder={Keyboard.dismiss} style={[styles.container, {...style}]}>
    <StatusBar backgroundColor={Pallete.primaryColor} barStyle="light-content" />
    {isLoading ? loader() : children}
  </SafeAreaView>
);

const loader = () => {
  return (
    <View style={styles.loader}>
      <ActivityIndicator color={Pallete.primaryColor} size="large" />
    </View>
  );
};

const styles = StyleSheet.create({
  background: {top: 0, left: 0, right: 0, bottom: 0, position: 'absolute'},
  container: {flex: 1, backgroundColor: Pallete.primaryColor},
  contentKeyboard: {flex: 0.9},
  loader: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Container;
