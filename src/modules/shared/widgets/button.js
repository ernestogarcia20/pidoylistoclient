import React from 'react';

import {TouchableOpacity, ActivityIndicator, StyleSheet} from 'react-native';
import Text from './text';

const Button = ({
  text,
  isLoading,
  colorIndicator = 'white',
  colorText = 'white',
  onPress = () => {},
}) => {
  return (
    <TouchableOpacity style={styles.buttonColor} onPress={onPress}>
      {!isLoading ? (
        <Text fontSize={12} color={colorText} weight="bold">
          {text}
        </Text>
      ) : (
        <ActivityIndicator color={colorIndicator} size="small" />
      )}
    </TouchableOpacity>
  );
};

export default Button;
const styles = StyleSheet.create({
  buttonColor: {
    height: 45,
    marginVertical: 5,
    backgroundColor: '#B31816',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
