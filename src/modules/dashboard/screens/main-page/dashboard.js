import React, {Component} from 'react';
import {connect} from 'react-redux';
import DashboardLayout from './components/dashboard-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import {LOGIN} from '../../../utils/constants/constants-navigate';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleLogOut = () => {
    const {
      navigation: {navigate},
    } = this.props;

    navigate(LOGIN);
  };

  handleButton = namePage => {
    const {
      navigation: {navigate},
    } = this.props;

    navigate(namePage);
  };

  render() {
    const {user} = this.props;
    const {
      navigation: {openDrawer},
    } = this.props;
    return (
      <Container>
        <Header name="Pagina Principal" actionDrawer={openDrawer} menu />
        <DashboardLayout logOut={this.handleLogOut} handleButton={this.handleButton} user={user} />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(Dashboard);
