import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Text from '../../../../shared/widgets/text';
import Button from '../../../../shared/widgets/button';
import {COMPANIES_LIST} from '../../../../utils/constants/constants-navigate';

const Layout = ({user, logOut, handleButton}) => {
  return (
    <View style={styles.container}>
      <View style={styles.contentButton}>
        <TouchableOpacity style={styles.roundButton} onPress={() => handleButton(COMPANIES_LIST)}>
          <Text color="white">Food Court</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.roundButton}>
          <Text color="white">Cines</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.roundButton}>
          <Text color="white">Delivery</Text>
        </TouchableOpacity>
      </View>
      <Button text="Cerrar sesion" onPress={logOut} />
    </View>
  );
};

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentButton: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  roundButton: {
    width: 100,
    height: 60,
    backgroundColor: '#A7A7A7',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
  },
});
