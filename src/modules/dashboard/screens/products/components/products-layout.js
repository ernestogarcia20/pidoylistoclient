import React, {useEffect} from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity, Dimensions, Image, Platform} from 'react-native';
import Swiper from 'react-native-swiper';
import Text from '../../../../shared/widgets/text';
import Loader from '../../../../shared/widgets/loader';
import palette from '../../../../shared/assets/palette';

let masterRef = null;
let childRefMenu = null;
let childRefDrink = null;
let childRefDayMenu = null;
let refScrollPlates = null;

//const listMenu = ['Menu1', 'Bebidas2', 'Menu del dia3'];

const Layout = ({
  selectTabMaster,
  listMenu,
  selectTabChild,
  setValue,
  childTabsMenu,
  listPlate,
  listDrink,
  listMenuDay,
  childTabsDrink,
  childTabsMenuDay,
  loading,
  changeTabbedChild,
  onPressItem,
}) => {
  /** SETEA POR DEFECTO LOS SWIPER */
  useEffect(() => {
    const index = listMenu.indexOf(selectTabMaster);
    if (childRefMenu || childRefDrink || childRefDayMenu) {
      setTimeout(() => {
        if (index === 0 && listPlate.length > 0) childRefMenu.scrollTo(0);
        if (index === 1 && listDrink.length > 0) childRefDrink.scrollTo(0);
        if (index === 2 && listMenuDay.length > 0) childRefDayMenu.scrollTo(0);
      }, 250);
    }
  }, [selectTabMaster]);

  /** EFECTO DEL SCROLL HORIZONTAL */
  const scrolled = i => {
    if (!refScrollPlates) return false;
    refScrollPlates.scrollToOffset({
      animated: true,
      offset: (i * width) / 3.1,
    });
    return i;
  };

  /** SETEA EL ITEM SELECCIONADO Y MUEVE EL SWIPER */
  const buttonTabbedHandle = ({title, isMaster = true, listChild, refControl, firtsTap}) => {
    const scrollBy = isMaster ? listMenu.indexOf(title) : listChild.indexOf(title);
    if (!refControl) return;
    setValue(isMaster ? 'selectTabMaster' : 'selectTabChild', title);
    refControl.scrollTo(scrollBy);
    if (firtsTap) {
      changeTabbedChild(scrollBy);
    }
  };

  /** RENDER ITEM DEL PRODUCTO */


  const renderItemProduct = ({item}) => {
    return (
      <TouchableOpacity style={styles.item} onPress={()=>onPressItem(item) }>
        <View style={styles.contentImage}>
          <Image style={styles.image} source={{uri: item.strurl}} />
        </View>
        <View style={styles.contentDescription}>
          <Text color="black" fontSize={12}>
            {item.titulo}
          </Text>
          <View style={{paddingVertical: '7%'}}>
            <Text color="black" align="left" fontSize={12}>
              {item.descripcion}
            </Text>
          </View>
          <Text color="black" fontSize={12}>
            {item.precio}$
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  /** RENDER DEL ITEM DENTRO DEL SLIDE */
  const renderItemSlide = ({item}, i) => {
    const newList = item.data.filter(o => o.tipo === item.tipo);
    console.log(newList);
    return (
      <View style={styles.slides} key={i}>
        <FlatList
          scrollEnabled
          data={newList}
          renderItem={renderItemProduct}
          keyExtractor={(item1, index) => item1 + index}
        />
      </View>
    );
  };

  /** RENDER DEL CONTROL DE LOS BOTONES HORIZONTALES */
  const buttonTabbed = properties => {
    const {title, isMaster = true} = properties;
    const valStyle = isMaster ? selectTabMaster : selectTabChild;
    return (
      <TouchableOpacity
        onPress={() => buttonTabbedHandle(properties)}
        style={valStyle === title ? styles.buttonSelected : styles.button}>
        <Text color="white" fontSize={12}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      {/** MENU */}
      <View style={styles.listButton}>
        <FlatList
          horizontal
          style={styles.list}
          showsHorizontalScrollIndicator={false}
          data={listMenu}
          renderItem={({item}) =>
            buttonTabbed({title: item, refControl: masterRef, firtsTap: true})
          }
          keyExtractor={(item, index) => item + index}
        />
      </View>
      {!loading ? (
        <Swiper
          loop={false}
          ref={ref => {
            masterRef = ref;
          }}
          style={{flex:1}}
          scrollEnabled={false}
          showsButtons={false}
          showsPagination={false}>
          <View style={styles.slides}>
            {/* PLATOS  */}
            {childTabsMenu.length > 0 && (
              <View style={styles.listButton}>
                <FlatList
                  ref={ref => {
                    refScrollPlates = ref;
                  }}
                  horizontal
                  style={styles.list}
                  showsHorizontalScrollIndicator={false}
                  data={childTabsMenu}
                  renderItem={({item}) =>
                    buttonTabbed({
                      title: item,
                      isMaster: false,
                      listChild: childTabsMenu,
                      refControl: childRefMenu,
                    })
                  }
                  keyExtractor={(item, index) => item + index}
                />
              </View>
            )}
            {listPlate.length > 0 && (
              <Swiper
                style={styles.wrapper}
                ref={ref => {
                  childRefMenu = ref;
                }}
                onIndexChanged={i => {
                  const text = childTabsMenu[i];
                  setValue('selectTabChild', text);
                  scrolled(i - 1);
                }}
                loop={false}
                showsButtons={false}
                showsPagination={false}>
                {listPlate.map((item, i) => {
                  return renderItemSlide({item}, i);
                })}
              </Swiper>
            )}
          </View>
          <View style={styles.slides}>
            {/* DRINKS  */}
            {listDrink.length > 0 ? (
              <View style={styles.listButton}>
                <FlatList
                  horizontal
                  style={styles.list}
                  showsHorizontalScrollIndicator={false}
                  data={childTabsDrink}
                  renderItem={({item}) =>
                    buttonTabbed({
                      title: item,
                      isMaster: false,
                      listChild: childTabsDrink,
                      refControl: childRefDrink,
                    })
                  }
                  keyExtractor={(item, index) => item + index}
                />
              </View>
            ) : (
              <View />
            )}
            {listDrink.length > 0 && (
              <Swiper
                loop={false}
                style={styles.wrapper}
                ref={ref => {
                  if(!ref) return;
                  childRefDrink = ref;
                }}
                showsButtons={false}
                showsPagination={false}>
                {listDrink.map(item => {
                  return renderItemSlide({item});
                })}
              </Swiper>
            )}
          </View>
          <View style={styles.slides}>
            {/** MENU DEL DIA */}
            <View style={styles.listButton}>
              {childTabsMenuDay.length > 0 && (
                <FlatList
                  horizontal
                  style={styles.list}
                  showsHorizontalScrollIndicator={false}
                  data={childTabsMenuDay}
                  renderItem={({item}) =>
                    buttonTabbed({
                      title: item,
                      isMaster: false,
                      listChild: childTabsMenuDay,
                      refControl: childRefDayMenu,
                    })
                  }
                  keyExtractor={(item, index) => item + index}
                />
              )}
            </View>
            {listMenuDay.length > 0 && (
              <Swiper
                style={styles.wrapper}
                loop={false}
                ref={ref => {
                  childRefDayMenu = ref;
                }}
                showsButtons={false}
                showsPagination={false}>
                {listMenuDay.map(item => {
                  return renderItemSlide({item});
                })}
              </Swiper>
            )}
          </View>
        </Swiper>
      ) : (
        <View style={styles.contentLoader}>
          <Loader />
        </View>
      )}
    </View>
  );
};

export default Layout;
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  slides: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  listButton: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: palette.primaryColor,
  },
  button: {
    width: width / 3.1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    padding: 10,
  },
  buttonSelected: {
    width: width / 3.1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    backgroundColor: 'transparent',
  },
  list: {
    flex: 1,
    flexDirection: 'row',
  },
  item: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    height: 140,
    marginVertical: 0,
  },
  contentImage: {
    flex: 0.2,
    alignItems: 'center',
    paddingHorizontal: '6%',
  },
  contentDescription: {
    flex: 0.6,
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  image: 
  {width: 150, height: 130,  resizeMode: 'stretch'},
  contentLoader: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  wrapper: Platform.OS === 'ios' ? {} : {width,height:height*0.9},
});
