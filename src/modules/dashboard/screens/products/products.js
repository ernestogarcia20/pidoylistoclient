import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import ProductsLayout from './components/products-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import {getProductsByCompany} from '../../services/services-dashboard';
import {PRODUCT_SELECTED} from '../../../utils/constants/constants-navigate';

class Products extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      idempresas: 0,
      selectTabMaster: 'Menu',
      selectTabChild: 'Plato',
      childTabsMenu: [],
      childTabsDrink: [],
      childTabsMenuDay: [],
      listMenu: [],
      listPlate: [],
      listDrink: [],
      listMenuDay: [],
      loading: true,
    };
  }

  //es igual al onload
  componentDidMount() {
    const {
      navigation: {getParam},
      productList,
    } = this.props;
    this.setState({idempresas: getParam('idempresas', 1)}, async () => {
      await this.getProducts();
      if (productList.length > 0) await this.setList();
    });
  }
  /**
   *
   * CUANDO HAY ALGUN CAMBIO EN EL CICLO DE VIDA, EJECUTA LA SIGUIENTE VALIDACION
   */

  //si cambia una variable en el store pasa por aqui. 
  componentDidUpdate(props) {
    const {productList, checkInternet} = this.props;
    if (props.productList !== productList || checkInternet !== props.checkInternet) {
      if (props.checkInternet && productList.length === 0) this.getProducts();
      this.setList();
    }
  }

  /** OBTIENE LOS PRODUCTOS Y LOS SETEA EN EL STORE */
  getProducts = () => {
    const {dispatch} = this.props;
    const {idempresas} = this.state;
    getProductsByCompany(dispatch, idempresas).catch(e => {
      this.setState({error: e});
    });
    
  };

  /** CAPTURA LA LISTA SETEADA EN getProducts Y SETEA LAS LISTA EN EL ESTADO */
  setList = () => {
    const {productList} = this.props;

    if (productList.length < 1 || !productList || productList === undefined) {
      this.setState({loading: false});
      return false;
    }
    const MenuArriba = productList[0].TODOARRIBA;
    const listMenuGet = MenuArriba.split(",");
    const getListMenu = this.newListVal(productList, 'menu').filter(o => !o.esdeldia && !o.bebida);
    const getListDrink = this.newListVal(productList, 'drink');
    const getListMenuDay = this.newListVal(productList, 'esdeldia');

    let listKeysMenu = [];
    let listGroupMenu = [];

    let menuList = [];
    let drinkList = [];
    let menuDayList = [];

    let listKeysDrink = [];
    let listGroupDrink = [];

    let listKeysMenuDay = [];
    let listGroupMenuDay = [];

    if (getListMenu.length > 0) {
      menuList = this.groupBy(getListMenu, key => key.tipo);
      listKeysMenu = Array.from(menuList.keys());
      listGroupMenu = this.newListFormat(Array.from(menuList.values()));
    }

    if (getListDrink.length > 0) {
      drinkList = this.groupBy(getListDrink, key => key.tipo);
      listKeysDrink = Array.from(drinkList.keys());
      listGroupDrink = this.newListFormat(Array.from(drinkList.values()));
    }

    if (getListMenuDay.length > 0) {
      menuDayList = this.groupBy(getListMenuDay, key => key.tipo);
      listKeysMenuDay = Array.from(menuDayList.keys());
      listGroupMenuDay = this.newListFormat(Array.from(menuDayList.values()));
    }

    console.log(listGroupMenu);

    /** LE DA UN TIEMPO AL JS DE RENDERIZAR LAS VISTAS */
    setTimeout(() => {
      this.setState({
        listMenu:listMenuGet,
        listPlate: listGroupMenu,
        childTabsMenu: listKeysMenu,
        selectTabChild: listKeysMenu[0],
        listDrink: listGroupDrink,
        childTabsDrink: listKeysDrink,
        listMenuDay: listGroupMenuDay,
        childTabsMenuDay: listKeysMenuDay,
        loading: false,
      });
    }, 350);
    return productList;
  };

  /** FILTRA LA LISTA POR MENU, BEBIDAS, MENU DEL DIA */
  newListVal = (array, filter) => {
    const newList = [];
    array.map(item => {
      if (!item.esdeldia && !item.bebida && filter === 'menu') return newList.push(item);
      if (!item.esdeldia && filter === 'esdeldia') return false;
      if (item.esdeldia && filter === 'esdeldia') return newList.push(item);
      if (!item.bebida && filter !== 'esdeldia') return false;
      if (!item.esdeldia && item.bebida && filter !== 'esdeldia') return newList.push(item);
      return newList.push(item);
    });
    return newList;
  };

  /** SE CREA LA LISTA PARA EL SWIPER */
  newListFormat = array => {
    const newList = [];
    array.map(e => {
      return newList.push({data: e, tipo: e[0].tipo});
    });
    return newList;
  };

  /** AGRUPA LOS KEY Y LA LISTA */
  groupBy = (list, keyGetter) => {
    const map = new Map();
    list.forEach(item => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  };

  /** CAMBIA EN EL ESTADO selectTabChild POR DEFECTO */
  changeTabbedChild = index => {
    const {childTabsMenu, childTabsDrink, childTabsMenuDay} = this.state;
    let changeText = 'Plato';
    if (index === 0) changeText = String(childTabsMenu[0]);
    if (index === 1) changeText = String(childTabsDrink[0]);
    if (index === 2) changeText = String(childTabsMenuDay[0]);

    this.setState({selectTabChild: changeText});
  };

  handleChange = (name, value) => this.setState({[name]: value});


  onPressItem = (item) => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(PRODUCT_SELECTED, {itemSelected: item});
  };

  render() {
    const {
      navigation: {goBack},
      checkInternet,
      user,

    } = this.props;
    return (
      <Container>
        <Header name="Lista de Productos" actionDrawer={() => goBack()} />
        <ProductsLayout
          checkInternet={checkInternet}
          serachFilter={this.searchFilterFunction}
          {...this.state}
          user={user}
          changeTabbedChild={this.changeTabbedChild}
          setValue={this.handleChange}
          onPressItem = {this.onPressItem}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
  productList: state.products.productList,
});

export default connect(mapStateToProps)(Products);
