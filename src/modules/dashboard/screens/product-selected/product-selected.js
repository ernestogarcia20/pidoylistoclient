import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import ProductSelectedLayout from './components/product-selected-layout';


class Product_selected extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
          idempresas: 0,
           notas:"",
           nombrecomercio:"",
           tituloprod:"",
           descripcionprod:"",
           precioprod:"",
           urlprod:"",
           
        };
    }
   componentDidMount(){
    const {
        navigation: {getParam},
      } = this.props;
      const itemSelected = getParam("itemSelected");
      this.setState({
        tituloprod:itemSelected.titulo,
        descripcionprod:itemSelected.descripcion,
        precioprod:itemSelected.precio,
        urlprod:itemSelected.strurl,

      })

   } 
   
   handleChange = (name, value) => this.setState({[name]: value});
    render(){
        return <ProductSelectedLayout setValue={this.handleChange} {...this.state}/>;
    }

}
export default Product_selected;