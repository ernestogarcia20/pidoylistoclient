import React from 'react';
import {View,StyleSheet,Text,TextInput,FlatList, TouchableOpacity, Dimensions, Image, Platform} from 'react-native';




const createLayout = ({tituloprod,descripcionprod,precioprod,urlprod,nombrecomercio,notas,setValue}) =>{
    return(
        <View styles={styles.container}>
           <View style={styles.Title}>
                <Text>Carta</Text>
           </View> 
           <View style={styles.Title}>
             <Text>{nombrecomercio}</Text>
           </View> 
           <View style={styles.Title}>
              <Image style={styles.ImageProduct} source={{uri:urlprod}}></Image>
           </View> 
           <View style={styles.Title}>
                 <Text>{descripcionprod}</Text>
           </View>
           <View style={styles.Title}>
                 <Text>{precioprod}</Text>
           </View>
           <View style={styles.Title}>
                 <TextInput value={notas} onChangeText={(Text) => {
                                setValue("notas",Text);

                     }
                 }></TextInput>
           </View>
        </View>  
        )
 }
 export default createLayout;

 const styles = StyleSheet.create({
     Title: {
        
        backgroundColor: 'white',
        alignContent:"center"
      },
      ImageProduct: {
        width: 100, height: 100,
        backgroundColor: 'white',
        alignContent:"center",
        resizeMode: 'stretch'
      },


    container: {
      flex: 1,
      backgroundColor: 'white',
    },
   
  });