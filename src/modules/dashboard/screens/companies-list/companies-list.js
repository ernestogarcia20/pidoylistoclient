import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import CompaniesListLayout from './components/companies-list-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import {getCompanyGeography} from '../../services/services-dashboard';
import {PRODUCTS} from '../../../utils/constants/constants-navigate';

class CompaniesList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      data: [],
      error: '',
    };
    this.arrayholder = [];
  }

  async componentDidMount() {
    await this.getCompanies();
    await this.setList();
  }
  /**
   *
   * CUANDO HAY ALGUN CAMBIO EN EL CICLO DE VIDA, EJECUTA LA SIGUIENTE VALIDACION
   */

  componentDidUpdate(props) {
    const {companiesList, checkInternet} = this.props;
    const {data} = this.state;
    if (props.companiesList !== companiesList || checkInternet !== props.checkInternet) {
      if (props.checkInternet && (data.length === 0 || companiesList.length === 0))
        this.getCompanies();
      this.setList();
    }
  }

  getCompanies = () => {
    const {dispatch} = this.props;
    getCompanyGeography(dispatch).catch(e => {
      this.setState({error: e});
    });
  };

  setList = () => {
    const {companiesList} = this.props;
    this.arrayholder = companiesList;
    setTimeout(() => {
      this.setState({data: companiesList});
    }, 300);
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });

    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.nombre.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };

  onPressItem = item => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(PRODUCTS, {idempresas: item.idempresas});
  };

  render() {
    const {user} = this.props;
    const {
      navigation: {goBack},
      companiesList,
      checkInternet,
    } = this.props;
    return (
      <Container isLoading={companiesList.length <= 0}>
        <Header name="Lista de Empresas" actionDrawer={() => goBack()} />
        <CompaniesListLayout
          checkInternet={checkInternet}
          companiesList={companiesList}
          serachFilter={this.searchFilterFunction}
          {...this.state}
          user={user}
          onPressItem={this.onPressItem}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  companiesList: state.companies.companiesList,
  checkInternet: state.checkInternet.checkInternet,
});

export default connect(mapStateToProps)(CompaniesList);
