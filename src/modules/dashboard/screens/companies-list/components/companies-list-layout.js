import React from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity, Image} from 'react-native';
import {HeaderClassicSearchBar} from 'react-native-header-search-bar';
import Text from '../../../../shared/widgets/text';
import Loader from '../../../../shared/widgets/loader';
import {BASE_API} from '../../../../shared/services/api-service';
import {URL_IMAGE_RESTAURANTS} from '../../../../utils/constants/url-constants';

const renderSeparator = () => {
  return <View style={styles.separator} />;
};

const Layout = ({onPressItem, data, serachFilter, value, checkInternet}) => {
  const renderHeader = () => {
    return (
      <HeaderClassicSearchBar
        value={value}
        searchBoxText="Buscar"
        onChangeText={text => serachFilter(text)}
      />
    );
  };
  const emptyComponent = () => {
    return <View style={styles.loader}>{checkInternet && data.length === 0 && <Loader />}</View>;
  };
  const renderItem = ({item}) => {
    const {nombrecomercio, url} = item;
    return (
      <TouchableOpacity style={styles.item} onPress={() => onPressItem(item)}>
        <View style={styles.contentImage}>
          <Image
            style={styles.image}
            //source={{uri: `${BASE_API}/${URL_IMAGE_RESTAURANTS}/${url}`}}
            source={{uri: `${url}`}}
          />
        </View>
        <View style={styles.contentDescription}>
          <Text color="black">{nombrecomercio}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      {renderHeader()}
      <FlatList
        style={styles.list}
        data={data}
        renderItem={renderItem}
        keyExtractor={(item, index) => item + index}
        ItemSeparatorComponent={renderSeparator}
        ListEmptyComponent={emptyComponent}
      />
    </View>
  );
};
export default Layout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  item: {
    paddingHorizontal: 30,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 140,
  },
  list: {
    marginTop: 10,
  },
  contentImage: {
    flex: 0.3,
    alignItems: 'center',
  },
  contentDescription: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  separator: {
    height: 1,
    width: '86%',
    backgroundColor: '#CED0CE',
    marginLeft: '14%',
  },
  image: {width: 150, height: 140},
});
