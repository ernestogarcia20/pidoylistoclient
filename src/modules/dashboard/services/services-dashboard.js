import API from '../../shared/services/api-service';

import {URL_DINERS} from '../../utils/constants/url-constants';
import {SET_COMPANIES} from '../reducers/company';
import { SET_PRODUCTS } from '../reducers/products';

export const getCompanyGeography = dispatch => {
  const data = {
    longitud: "-79.51761930"
     ,latitud :"8.99964200"
     ,tipo : 0	,
  };
  return API.post(`${URL_DINERS}/GetCompanyGeography`, data).then(response => {
    if (response === undefined || !response) return setDispatch(dispatch, [], SET_COMPANIES);
    setDispatch(dispatch, response, SET_COMPANIES);
    return response;
  });
};

export const getProductsByCompany = async (dispatch, idCompany) => {
  const data = {
    idempresas: idCompany,
  };
  const response = await API.post(`${URL_DINERS}/GetProductsByCompany`, data);
  if (response === undefined || !response) return false;
  setDispatch(dispatch, response, SET_PRODUCTS);
  return response;
};

export const setDispatch = (dispatch, data, type) => {
  dispatch({
    type,
    payload: data,
  });
};
