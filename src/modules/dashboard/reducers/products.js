/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_PRODUCTS = 'SET_PRODUCTS';
export const REMOVE_PRODUCTS = 'REMOVE_PRODUCTS';
export const REMOVE_ALL = 'REMOVE_ALL';

const INITIAL_STATE = {
  productList: [],
};
function user(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_PRODUCTS:
      return {...state, productList: action.payload};
    case REMOVE_PRODUCTS:
      return {...state, productList: []};
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export default user;
