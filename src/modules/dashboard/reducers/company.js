/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_COMPANIES = 'SET_COMPANIES';
export const REMOVE_COMPANIES = 'REMOVE_COMPANIES';

const INITIAL_STATE = {
  companiesList: [],
};
function user(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_COMPANIES:
      return {...state, companiesList: action.payload};
    case REMOVE_COMPANIES:
      return false;
    default:
      return state;
  }
}

export default user;
