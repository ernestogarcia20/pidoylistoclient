import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet, Image, KeyboardAvoidingView} from 'react-native';
import Logo from '../../../../shared/assets/background/max_delivery_logo.png';
import {FloatingTitleTextInputField} from '../../../../shared/widgets/input-floating';
import Botton from '../../../../shared/widgets/button';

const Layout = ({
  handleSignIn,
  setState,
  email,
  password,
  isLoading,
  handleSignUp,
  handleFacebook,
}) => {
  return (
    <View style={styles.container} pointerEvents={isLoading ? 'none' : 'auto'}>
      <KeyboardAvoidingView
        style={styles.contentForm}
        {...Platform.select({
          android: {behavior: 'padding'},
          ios: {behavior: 'padding'},
        })}
        enabled>
        <View style={styles.contentLogo}>
          <Image source={Logo} style={styles.logo} />
        </View>
        <View style={styles.contentInputs}>
          <FloatingTitleTextInputField
            attrName="email"
            title="Email"
            value={email}
            updateMasterState={setState}
            textInputStyles={styles.input}
            keyboardType="email-address"
          />
          <FloatingTitleTextInputField
            attrName="password"
            title="Password"
            value={password}
            updateMasterState={setState}
            textInputStyles={styles.input}
            otherTextInputProps={{
              secureTextEntry: true,
            }}
          />
        </View>
      </KeyboardAvoidingView>
      <View style={styles.contentButtons}>
        <Botton text="Ingresar" isLoading={isLoading} onPress={handleSignIn} />
        <Botton text="Registrar" onPress={handleSignUp} />
        <Botton text="Registrate con Facebook" onPress={handleFacebook} />
      </View>
    </View>
  );
};
export default Layout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentLogo: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonColor: {
    height: 45,
    marginVertical: 5,
    backgroundColor: '#B31816',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 40,
    color: 'black',
  },
  logo: {
    height: 192,
    width: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  whiteText: {
    color: 'white',
    fontWeight: 'bold',
  },
  contentForm: {
    flex: 0.8,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  contentButtons: {
    flex: 0.3,
  },
  contentInputs: {
    flex: 0.25,
    justifyContent: 'flex-end',
  },
});
