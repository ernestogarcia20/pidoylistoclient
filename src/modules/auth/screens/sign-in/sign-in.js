import React, {Component} from 'react';
import {connect} from 'react-redux';
import SignInLayout from './components/sign-in-layout';
import Container from '../../../shared/widgets/container';
import {signIn} from '../../services/user-service';
import {DRAWER, SIGN_UP} from '../../../utils/constants/constants-navigate';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {email: 'egarcia@oriomka.com', password: '1234', isLoading: false};
  }

  Handle = () => {
    const {email, password} = this.state;
    this.setState({isLoading: true});
    const {
      navigation: {navigate},
      dispatch,
    } = this.props;
    const data = {
      user: email,
      password,
    };
    signIn(dispatch, data).then(() => {
      this.setState({isLoading: false});
      navigate(DRAWER);
    });
  };

  handleSignUp = () => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(SIGN_UP);
  };

  handleFacebook = () => {};

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {email, password, isLoading} = this.state;
    return (
      <Container>
        <SignInLayout
          email={email}
          password={password}
          isLoading={isLoading}
          handleSignUp={this.handleSignUp}
          handleFacebook={this.handleFacebook}
          handleSignIn={this.Handle}
          setState={this.handleChange}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(Login);
