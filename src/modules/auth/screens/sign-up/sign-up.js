import React, {Component} from 'react';
import {connect} from 'react-redux';
import SignUpLayout from './components/sign-up-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import {signUp} from '../../services/user-service';
import {DASHBOARD} from '../../../utils/constants/constants-navigate';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {email: '', password: '', confirmPassword: '', name: '', lastName: '', phone: ''};
  }

  Handle = () => {
    const {email, password} = this.state;
    this.setState({isLoading: true});
    const {
      navigation: {navigate},
      dispatch,
    } = this.props;
    const data = {
      user: email,
      password,
    };
    signUp(dispatch, data).then(() => {
      this.setState({isLoading: false});
      navigate(DASHBOARD);
    });
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {navigation:{goBack}} = this.props;
    return (
      <Container>
        <Header name="Registro" actionDrawer={() => goBack()} />
        <SignUpLayout {...this.state} handleSignUp={this.Handle} setState={this.handleChange} />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(SignUp);
