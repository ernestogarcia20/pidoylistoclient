import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView} from 'react-native';
import {FloatingTitleTextInputField} from '../../../../shared/widgets/input-floating';
import Botton from '../../../../shared/widgets/button';

const Layout = ({
  handleSignUp,
  setState,
  email,
  password,
  confirmPassword,
  name,
  lastName,
  phone,
  isLoading,
}) => {
  return (
    <View style={styles.container} pointerEvents={isLoading ? 'none' : 'auto'}>
      <KeyboardAvoidingView
        style={styles.contentForm}
        {...Platform.select({
          android: {behavior: 'padding'},
          ios: {behavior: 'padding'},
        })}
        enabled>
        <View style={styles.contentInputsHorizontal}>
          <FloatingTitleTextInputField
            attrName="email"
            title="Email"
            value={email}
            updateMasterState={setState}
            textInputStyles={styles.input}
            keyboardType="email-address"
          />
        </View>
        <View style={styles.contentInputsHorizontal}>
          <FloatingTitleTextInputField
            attrName="password"
            title="Contraseña"
            value={password}
            updateMasterState={setState}
            textInputStyles={styles.input}
            otherTextInputProps={{
              secureTextEntry: true,
            }}
          />
          <FloatingTitleTextInputField
            attrName="confirmPassword"
            title="Confirmacion"
            value={confirmPassword}
            updateMasterState={setState}
            textInputStyles={styles.input}
            otherTextInputProps={{
              secureTextEntry: true,
            }}
          />
        </View>
        <View style={styles.contentInputsHorizontal}>
          <FloatingTitleTextInputField
            attrName="name"
            title="Nombre"
            value={name}
            updateMasterState={setState}
            textInputStyles={styles.input}
          />
          <FloatingTitleTextInputField
            attrName="lastName"
            title="Apellido"
            value={lastName}
            updateMasterState={setState}
            textInputStyles={styles.input}
          />
        </View>
        <View style={styles.contentInputsHorizontal}>
          <FloatingTitleTextInputField
            attrName="phone"
            title="Telefono"
            value={phone}
            updateMasterState={setState}
            textInputStyles={styles.input}
            keyboardType="phone-pad"
          />
        </View>
      </KeyboardAvoidingView>
      <View style={styles.contentButtons}>
        <Botton text="Registrar" onPress={handleSignUp} />
      </View>
    </View>
  );
};
export default Layout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentLogo: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonColor: {
    height: 45,
    marginVertical: 5,
    backgroundColor: '#B31816',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 40,
    color: 'black',
  },
  logo: {
    height: 192,
    width: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  whiteText: {
    color: 'white',
    fontWeight: 'bold',
  },
  contentForm: {
    flex: 0.8,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  contentButtons: {
    flex: 0.3,
  },
  contentInputs: {
    flex: 0.2,
    justifyContent: 'center',
  },
  contentInputsHorizontal: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
