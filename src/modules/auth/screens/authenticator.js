import React from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';
import {DRAWER, LOGIN} from '../../utils/constants/constants-navigate';

class Authenticator extends React.Component {
  constructor(props) {
    super(props);
    this.bootstrapValidation();
  }

  bootstrapValidation = () => {
    const {
      navigation: {navigate},
      user,
    } = this.props;
    navigate(user ? DRAWER : LOGIN);
  };

  render = () => <View />;
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(Authenticator);
