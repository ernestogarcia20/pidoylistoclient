import API from '../../shared/services/api-service';
import {SET_USER} from '../reducers/user';
import {URL_SECURITY} from '../../utils/constants/url-constants';

export const signIn = (dispatch, data) => {
  return API.post(`${URL_SECURITY}/Authentication`, data).then(response => {
    if (response === undefined || !response) return false;
    const user = response.ListData[0];
    setDispatch(dispatch, user, SET_USER);
    if (response.id_token) API.setAuthorization(response.id_token);
    return response;
  });
};

export const signUp = (dispatch, data) => {
  return '';
};

export const setDispatch = (dispatch, data, type) => {
  dispatch({
    type,
    payload: data,
  });
};
