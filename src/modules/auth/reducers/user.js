/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_USER = 'SET_USER';
export const REMOVE_USER = 'REMOVE_USER';

/**
 * @return {boolean | Object}
 */
function user(state = false, action) {
  switch (action.type) {
    case SET_USER:
      return { ...action.payload };
    case REMOVE_USER:
      return false;
    default:
      return state;
  }
}

export default user;
