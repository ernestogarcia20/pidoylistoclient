import React from 'react';
import {View, StyleSheet, Image, FlatList, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Text from '../../shared/widgets/text';

const renderItem = (item, onPressItem) => {
  return (
    <TouchableOpacity style={styles.listItem} onPress={() => onPressItem(item.name)}>
      <Icon name={item.icon} size={32} />
      <Text style={styles.title}>{item.name}</Text>
    </TouchableOpacity>
  );
};

const Layout = ({user, routes, onPressItem}) => {
  return (
    <View style={styles.container}>
      <Image style={styles.profileImg} />
      <Text color="gray" weight="bold" fontSize={16} style={{marginTop: 10}}>
        {user.nombre} {user.apellido}
      </Text>
      <Text color="gray" style={{marginBottom: 10}}>
        {user.email}
      </Text>
      <View style={styles.sidebarDivider} />
      <FlatList
        style={{width: '100%', marginLeft: 30}}
        data={routes}
        renderItem={({item}) => renderItem(item, onPressItem)}
        keyExtractor={item => item.name}
      />
    </View>
  );
};
export default Layout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileImg: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginTop: 20,
  },
  listItem: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    marginLeft: 20,
  },
  sidebarDivider: {
    height: 1,
    width: '100%',
    backgroundColor: 'lightgray',
    marginVertical: 10,
  },
});
