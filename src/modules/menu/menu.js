import React, {Component} from 'react';
import {connect} from 'react-redux';
import Container from '../shared/widgets/container';
import MenuLayout from './components/menu-layout';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      routes: [
        {
          name: 'Dashboard',
          icon: 'home',
        },
        {
          name: 'Profile',
          icon: 'person',
        },
        {
          name: 'Settings',
          icon: 'settings',
        },
      ],
    };
  }

  onPressItem = name => {
    const {
      navigation: {navigate, closeDrawer},
    } = this.props;
    navigate(name);
    closeDrawer();
  };

  render() {
    const {routes} = this.state;
    const {user} = this.props;
    return (
      <Container>
        <MenuLayout user={user} onPressItem={this.onPressItem} routes={routes} />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(Menu);
