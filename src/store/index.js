import {applyMiddleware, createStore} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';
import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers';

import storage from 'redux-persist/lib/storage';
import reducer from './reducers';

const persistConfig = {
  key: 'root',
  storage,
  // blacklist: ['navigation'],
};

const middleware = createReactNavigationReduxMiddleware(state => state.navigation, 'root');

const store = createStore(persistReducer(persistConfig, reducer), applyMiddleware(middleware));

const persistor = persistStore(store);

export {store, persistor};
