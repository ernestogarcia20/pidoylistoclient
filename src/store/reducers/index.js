import { combineReducers } from 'redux';

import user from '../../modules/auth/reducers/user';
import companies from '../../modules/dashboard/reducers/company';
import products from '../../modules/dashboard/reducers/products';
import navigation from './navigation';
import app from './app';
import checkInternet from './check-internet';

export default combineReducers({
  app,
  navigation,
  user,
  companies,
  checkInternet,
  products,
});
