import {createSwitchNavigator} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import Menu from './modules/menu/menu';
import Authenticator from './modules/auth/screens/authenticator';
import Login from './modules/auth/screens/sign-in/sign-in';
import Dashboard from './modules/dashboard/screens/main-page/dashboard';
import CompaniesList from './modules/dashboard/screens/companies-list/companies-list';
import Products from './modules/dashboard/screens/products/products';
import ProductSelect from './modules/dashboard/screens/product-selected/product-selected';
import SignUp from './modules/auth/screens/sign-up/sign-up';
import {LOGIN, AUTHENTICATOR} from './modules/utils/constants/constants-navigate';

const DashboardStack = createStackNavigator(
  {
    // For header options
    Dashboard,
    CompaniesList,
    Products,
    ProductSelect,
  },
  {
    navigationOptions: {
      header: null,
    },
    headerMode: 'none',
  }
);

const Drawer = createDrawerNavigator(
  {
    Dashboard: DashboardStack,
  },
  {
    unmountInactiveRoutes: true,
    headerMode: 'none',
    contentComponent: Menu,
  }
);

const Auth = createStackNavigator(
  {
    Login,
    SignUp,
  },
  {
    initialRouteName: LOGIN,
    headerMode: 'none',
  }
);

export default createSwitchNavigator(
  {
    Authenticator,
    Login: Auth,
    Drawer,
  },
  {
    initialRouteName: AUTHENTICATOR,
  }
);
